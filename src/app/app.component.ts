import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  toggleLyricsSetting = false;
  toggleLyricsClickTimes = [];
  date = new Date();

  toggleLyrics() {
    this.date = new Date();
    this.toggleLyricsClickTimes.push(this.date);
    console.log('Date: ' + this.toggleLyricsClickTimes)
    if (this.toggleLyricsSetting === false) {
      this.toggleLyricsSetting = true;
    } else {
      this.toggleLyricsSetting = false;
    }
  }
}
